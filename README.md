# Plagiarism Project #


In this project, we build a plagiarism detector that examines a text file and performs binary classification; labeling that file as either *plagiarized* or *not*, depending on how similar that text file is to a provided source text. Detecting plagiarism is an active area of research; the task is non-trivial and the differences between paraphrased answers and original work are often not so obvious.

We create containment and longest common subsequence (LCS) [feaures](https://s3.amazonaws.com/video.udacity-data.com/topher/2019/January/5c412841_developing-a-corpus-of-plagiarised-short-answers/developing-a-corpus-of-plagiarised-short-answers.pdf) as similarity feature between documents. Then we create a binary classifier using feedforward neural network to classify plagiarized and non-plagiarized document. We use Pytorch to develop the neural network, then train and deploy the model using AWS. 

Below is an example of longest common subsequence (LCS).

![Loss Plot](notebook_ims/common_subseq_words.png)

The equation below is how to calculate the containment feature.

![Loss Plot](notebook_ims/containment.png)


### File Structure ###
```
- source/model.py # the neural network model
- source/predict.py # entry point for inference
- source/train.py # entry point for training
- 1_Data_Exploration.ipynb # Explore the existing data features and the data distribution.
- 2_Plagiarism_Feature_Engineering.ipynb # extract similarity features
- 3_Training_a_Model.ipynb
- README.md
```

